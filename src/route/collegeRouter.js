import { Router } from "express";
import {
  createCollege,
  deleteCollege,
  readAllCollege,
  readSpecificCollege,
  updateCollege,
} from "../controller/collegeController.js";

export let collegeRouter = Router();

collegeRouter.route("/").post(createCollege).get(readAllCollege);

collegeRouter
  .route("/:id") //localhost:8000/students/1234124
  .get(readSpecificCollege)
  .patch(updateCollege)
  .delete(deleteCollege);

/* 
  Student.create(data)
  Student.find({})
  Student.findById(id)
  Student.findByIdAndUpdate(id,data)
  Student.findByIdAndDelete(id)
  
  
  */
