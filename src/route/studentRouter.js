import { Router } from "express";
import { Student } from "../schema/model.js";
import {
  createStudent,
  deleteStudent,
  getSpecificStudent,
  getStudent,
  updateStudent,
} from "../controller/studentController.js";

export let studentRouter = Router();

studentRouter.route("/").post(createStudent).get(getStudent);

studentRouter
  .route("/:id") //localhost:8000/students/1234124
  .get(getSpecificStudent)
  .patch(updateStudent)
  .delete(deleteStudent);

/* 
  Student.create(data)
  Student.find({})
  Student.findById(id)
  Student.findByIdAndUpdate(id,data)
  Student.findByIdAndDelete(id)
  
  
  */
