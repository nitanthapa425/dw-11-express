import { Router } from "express";
import {
  createContact,
  deleteContact,
  readAllContact,
  readSpecificContact,
  updateContact,
} from "../controller/contactController.js";

export let contactRouter = Router();

contactRouter.route("/").post(createContact).get(readAllContact);

contactRouter
  .route("/:id") //localhost:8000/students/1234124
  .get(readSpecificContact)
  .patch(updateContact)
  .delete(deleteContact);

/* 
  Student.create(data)
  Student.find({})
  Student.findById(id)
  Student.findByIdAndUpdate(id,data)
  Student.findByIdAndDelete(id)
  
  
  */
