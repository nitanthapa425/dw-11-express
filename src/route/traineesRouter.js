import { Router } from "express";
export let traineesRouter = Router();

traineesRouter
  .route("/") //localhost:8000/trainees
  .post((req, res, next) => {
    console.log(req.query);
    res.json({
      success: true,
      message: "trainees created successfully.",
    });
  })
  .get((req, res, next) => {
    res.json({
      success: true,
      message: "trainees read successfully.",
    });
    res.json("hello");
  })
  .patch((req, res, next) => {
    res.json({
      success: true,
      message: "trainees updated successfully.",
    });
  })
  .delete((req, res, next) => {
    res.json({
      success: true,
      message: "trainees deleted successfully.",
    });
  });

traineesRouter
  .route("/:id/a/:name") //localhost:8000/trainees/any1/a/any2
  .get((req, res, next) => {
    console.log(req.params);
    res.json({
      succuss: true,
    });
  })
  .post((req, res, next) => {
    console.log(req.body);
    console.log(req.query);
    console.log(req.params);
    res.json({
      success: true,
    });
  });

/* 

dynamic route params
{
  id:"123",
  name:"nitan",
  address:"gagal"
}




url = localhost:8000/nitan/gagal?age=29&college=nec&isMarried=false

url = route?query

*/
