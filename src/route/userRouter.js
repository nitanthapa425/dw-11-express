import { Router } from "express";
import {
  createUser,
  deleteUser,
  readAllUser,
  readSpecificUser,
  updateUser,
} from "../controller/userController.js";
// import { createUser, deleteUser, readAllUser, readSpecificUser, updateUser } from "../controller/userController.js";

export let userRouter = Router();

userRouter.route("/").post(createUser).get(readAllUser);

userRouter
  .route("/:id") //localhost:8000/students/1234124
  .get(readSpecificUser)
  .patch(updateUser)
  .delete(deleteUser);

/* 
  Student.create(data)
  Student.find({})
  Student.findById(id)
  Student.findByIdAndUpdate(id,data)
  Student.findByIdAndDelete(id)
  
  
  */
