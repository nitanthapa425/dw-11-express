import { Schema } from "mongoose";

export let studentSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required"],
    // lowercase: true,
    // uppercase:true,
    // trim: true,
    minLength: [3, "name must be at least 3 character long."],
    maxLength: [30, "name must be at most 30 character long."],

    // name must be alphabet  only

    validate: (value) => {
      // regex= pattern

      let onlyAlphabet = /^[A-Za-z]+$/.test(value);

      if (onlyAlphabet) {
      } else {
        throw new Error("name filed must have only alphabet");
      }
    },
  },
  phoneNumber: {
    type: Number,
    required: [true, "phoneNumber field is required."],
    trim: true,
    validate: (value) => {
      // min 10 character
      // max 10 character
      let strPhoneNumber = String(value);

      if (strPhoneNumber.length === 10) {
      } else {
        throw new Error("phoneNumber must be exact 10 character long.");
      }
    },
  },

  gender: {
    type: String,
    default: "male",
    required: [true, "gender field is required"],
    validate: (value) => {
      if (value === "male" || value === "female" || value === "other") {
      } else {
        let error = new Error("gender must be either male, female, other");
        throw error;
      }
    },
  },

  password: {
    type: String,
    required: [true, "password field is required"],

    // password must have 1 number, one lower case, one uppercase , one symbol, min 8 character, max 15 character
  },
  roll: {
    type: Number,
    required: [true, "roll field is required"],
    min: [50, "roll must be greater than or equal to 50"],
    max: [100, "roll must be less than or equal to  100"],
  },
  isMarried: {
    type: Boolean,
    required: [true, "isMarried field is required"],
  },
  spouseName: {
    type: String,
    required: [true, "spouseName field is required"],
  },
  email: {
    type: String,
    unique: true,
    required: [true, "email field is required"],
    validate: (value) => {
      let isValidEmail =
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value); //false
      if (isValidEmail) {
      } else {
        throw new Error("email must be valid");
      }
    },
  },
  dob: {
    type: Date,
    required: [true, "dob field is required"],
  },

  location: {
    country: {
      type: String,
      required: [true, "country field is required"],
    },
    exactLocation: {
      type: String,
      required: [true, "exactLocation field is required"],
    },
  },
  favTeacher: [
    {
      type: String,
    },
  ],
  favSubject: [
    {
      bookName: {
        type: String,
      },
      bookAuthor: {
        type: String,
      },
    },
  ],

  favNumber: [
    {
      type: Number,
    },
  ],
  favPerson: {
    name: {
      type: String,
    },
    age: {
      type: Number,
    },
  },
  favFood: [
    {
      name: {
        type: String,
      },
    },
  ],
});

/* 

schema
  manipulation
  validation
    inbuilt validation
      minLength, for string
      maxLength,  for string
      min, for number
      max for number
    custom validation
      validate:()=>{....}


*/

/* 
create =post
read = get
update =patch
delete = delete


*/
