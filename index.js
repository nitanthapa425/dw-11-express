import express, { json } from "express";
import { firstRouter } from "./src/route/firstRouter.js";
import { traineesRouter } from "./src/route/traineesRouter.js";
import { connectToMongodb } from "./src/connectToDb/connetToMongodb.js";
import { studentRouter } from "./src/route/studentRouter.js";
import { contactRouter } from "./src/route/contactRouter.js";
import { collegeRouter } from "./src/route/collegeRouter.js";
import { productRouter } from "./src/route/productRouter.js";
import { userRouter } from "./src/route/userRouter.js";
import { reviewRouter } from "./src/route/reviewRouter.js";
import jwt from "jsonwebtoken";
import { port, secretKey } from "./src/constan.js";
import { fileRouter } from "./src/route/fileRouter.js";

let expressApp = express();
expressApp.use(express.static("./public"));
expressApp.use(json());
connectToMongodb();

// expressApp.use((req, res, next) => {
//   console.log("i am application middlewarer");
//   next();
// });

expressApp.use("/first", firstRouter);
expressApp.use("/trainees", traineesRouter);
expressApp.use("/students", studentRouter);
expressApp.use("/contacts", contactRouter);
expressApp.use("/colleges", collegeRouter);
expressApp.use("/products", productRouter);
expressApp.use("/users", userRouter);
expressApp.use("/reviews", reviewRouter);
expressApp.use("/files", fileRouter);

expressApp.listen(port, () => {
  console.log("app is listening at port 8000");
});

// let password = "abc@1234";

// let hashPassword = await bcrypt.hash(password, 10);
// console.log(hashPassword);

// let hashPassword =
//   "$2b$10$ptHfjLyRz97RWPvuCcJWseJ7nvlW3qaM7PLV2VYleREPeerX1CAfa";
// let password = "12345678";

// let isPasswordMatch = await bcrypt.compare(password, hashPassword);
// console.log(isPasswordMatch);

/* 
make  backend application (using express)
attached port to that application

To make api we have two step
define router
use that route to the main file (index.js)

url:"localhost:8000", method:"get"   => i am home(get)


traines Router 
user that trail Router


Students =[
{name:"nitan",age:29, isMarried:false},
{name:"ram",age:30, isMarried:true},
{name:"hari",age:31, isMarried:false},
];


Define Object => Schema


Define array => model
  name,
  Object

*/

//college=>  name, location,
//schema
//model
//controller
//router
//index

// generate token
let infoObj = {
  _id: "12341241",
};

let expirifyInfo = {
  expiresIn: "365d",
};
let token = jwt.sign(infoObj, secretKey, expirifyInfo);
console.log(token);

// validate token
// let token =
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiIxMjM0MTI0MSIsImlhdCI6MTcwNjM0MTkzMCwiZXhwIjoxNzM3ODc3OTMwfQ.pCqrcSmo2wTE6KjPN8ske0-0C9ClsJFDxoS3ZNOgDUE";

// try {
//   let inObj = jwt.verify(token, "dw11");
//   console.log(inObj);
// } catch (error) {
//   console.log(error.message);
// }

// for token to be validate
// token must be made from the given secret key-
// token must not expire-

// if token is valid it gives infoObj else error

// .env file

/* 
localhost:8000/file
method post
res= {
  success:true,
  message:"file uploaded successfully."
}

*/

/* 
steps
send file from postman
save file to our sever

*/
